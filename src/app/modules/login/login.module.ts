import {NgModule} from '@angular/core';
import {LoginRouteModule} from './login.route';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';
import {GooglePlus} from '@ionic-native/google-plus/ngx';
import {Facebook} from '@ionic-native/facebook/ngx';
import {FingerprintAIO} from '@ionic-native/fingerprint-aio/ngx';
import {SecureStorage} from '@ionic-native/secure-storage/ngx';

@NgModule({
    imports: [
        LoginRouteModule
    ],
    providers: [
        InAppBrowser,
        GooglePlus,
        Facebook,
        FingerprintAIO,
        SecureStorage
    ]
})

export class LoginModule {
}
