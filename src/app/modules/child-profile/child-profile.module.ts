import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ChildDonateComponent} from './child-donate/child-donate.component';
import {ChildProfileMasterLayoutComponent} from './child-profile-master-layout/child-profile-master-layout';
import {AboutChilComponent} from './about-child/about-child.component';
import {ChildProfileRouteModule} from './child-profile.route';
import {TranslateModule} from '@ngx-translate/core';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'prefix',
        component: ChildProfileMasterLayoutComponent,
        children: [
            {
                path: 'about-child',
                pathMatch: 'full',
                component: AboutChilComponent
            },
            {
                path: 'child-donate',
                pathMatch: 'full',
                component: ChildDonateComponent
            },
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'about-child'
            }
        ]
    }
];

@NgModule({
    imports: [
        ChildProfileRouteModule,
        TranslateModule.forChild()
    ]
})

export class ChildProfileModule {
}
