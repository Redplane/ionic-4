import {Component, Inject} from '@angular/core';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';
import {IPaymentService} from '../../../services/interfaces/payment-service.interface';
import {LoadingController} from '@ionic/angular';
import {from} from 'rxjs';
import {flatMap, map} from 'rxjs/operators';
import {PAYMENT_SERVICE_INJECTION_TOKEN} from '../../../constants/injection-token.constant';

/**
 * Generated class for the LoginComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'child-donate',
    templateUrl: './child-donate.component.html',
    styleUrls: ['./child-donate.component.scss']
})
export class ChildDonateComponent {

    //#region Properties

    //#endregion

    //#region Constructor

    public constructor(protected browser: InAppBrowser,
                       @Inject(PAYMENT_SERVICE_INJECTION_TOKEN) protected paymentService: IPaymentService, protected loadingController: LoadingController) {
    }

    //#endregion

    //#region Methods

    /*
    * Called when donate button is clicked.
    * */
    public clickDonate(): void {

        // Display loading message
        from(this.loadingController.create({}))
            .pipe(
                flatMap(loadingMessage => {

                    // Display the loading message.
                    loadingMessage.present();

                    // Get url that payment gateway generates.
                    return this.paymentService
                        .buildPaymentRequestAsync();
                }),

                flatMap((paymentUrl: string) => {
                    const browserRef = this.browser.create(paymentUrl, 'self', {
                        fullscreen: 'yes'
                    });

                    return browserRef.on('loadstop');
                })
            )
            .subscribe(event => {
                console.log(event);
            });


    }

    //#endregion
}
