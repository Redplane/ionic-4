import { NgModule } from '@angular/core';
import { GuardianProfilePage } from './guardian-profile';
import {TranslateModule} from "@ngx-translate/core";
import {RouterModule, Routes} from "@angular/router";
import {IonicModule} from "@ionic/angular";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

const routes: Routes = [
  {
    path: '',
    component: GuardianProfilePage
  }
];

@NgModule({
  declarations: [
    GuardianProfilePage
  ],
  imports: [
    TranslateModule,
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    GuardianProfilePage
  ]
})

export class GuardianProfileModule {}
