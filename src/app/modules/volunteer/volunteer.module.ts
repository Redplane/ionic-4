import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {VolunteerComponent} from "./volunteer";
import {IonicModule} from "@ionic/angular";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

const routes: Routes = [
  {
    path: '',
    component: VolunteerComponent
  }
];

@NgModule({
  declarations: [
    VolunteerComponent
  ],
  imports: [
    IonicModule,
    FormsModule,
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    VolunteerComponent
  ]
})

export class VolunteerModule {}
