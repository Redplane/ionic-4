import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {LoadingController} from '@ionic/angular';
import {FirebaseMessaging} from '@ionic-native/firebase-messaging/ngx';

@Component({
    selector: 'app-start',
    templateUrl: 'app-start.component.html'
})
export class AppStartComponent {

    //#region Constructor

    public constructor(protected router: Router,
                       protected cloudMessagingService: FirebaseMessaging,
                       protected loadingController: LoadingController) {

    }

    //#endregion

    //#region Methods

    // Called when start button is clicked.
    public ngOnStartClicked(): void {

        let loaderControl = null;

        // Ask for permission.
        this.cloudMessagingService
            .getToken()
            .catch()
            .then((token: string) => {
                console.log(`Token = ${token}`);
                return this.loadingController.create({});
            })
            .then(loader => {

                // Temporarily save loader component.
                loaderControl = loader;

                // Present loader.
                loader.present();
                return this.router
                    .navigate(['/login'], {replaceUrl: true});
            })
            .then(_ => {
                loaderControl.dismiss();
            })
            .catch(_ => {
                loaderControl.dismiss();
            });
    }


    //#endregion
}
