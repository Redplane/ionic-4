import {Component} from '@angular/core';

/**
 * Generated class for the LoginComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'child-listing',
    templateUrl: './child-listing.html',
})
export class ChildListingPage {

    //#region Properties

    public users: Array<any>;

    //#endregion

    //#region Constructor

    public constructor() {
        this.users = new Array<any>();

        for (let i = 0; i < 12; i++) {
            this.users.push({
                photo: 'http://via.placeholder.com/512x512',
                username: `Username ${i}`
            });
        }
    }

    //#endregion

    //#region Method


    //#endregion
}
