import {NgModule} from '@angular/core';
import {MyFavoriteComponent} from './my-favorite';
import {TranslateModule} from "@ngx-translate/core";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
  {
    path: '',
    component: MyFavoriteComponent
  }
];

@NgModule({
  declarations: [
    MyFavoriteComponent
  ],
  imports: [
    TranslateModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    MyFavoriteComponent
  ]
})

export class MyFavoriteModule {
}
