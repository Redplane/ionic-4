import {NgModule} from '@angular/core';
import {MyTrackRecordsPage} from './my-track-records';
import {TranslateModule} from "@ngx-translate/core";
import {RouterModule, Routes} from "@angular/router";
import {IonicModule} from "@ionic/angular";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

const routes: Routes = [
  {
    path: '',
    component: MyTrackRecordsPage
  }
];

@NgModule({
  declarations: [
    MyTrackRecordsPage
  ],
  imports: [
    TranslateModule,
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    MyTrackRecordsPage
  ]
})

export class MyTrackRecordsModule {
}
