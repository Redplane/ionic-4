import {RouterModule, Routes} from '@angular/router';
import {TopDonatorComponent} from './top-donator.component';
import {TranslateModule} from '@ngx-translate/core';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {NgModule} from '@angular/core';

const routes: Routes = [
    {
        path: '',
        component: TopDonatorComponent
    }
];

@NgModule({
    imports: [
        TranslateModule.forChild(),
        IonicModule,
        FormsModule,
        CommonModule,
        RouterModule.forChild(routes)
    ],
    exports: [
        TopDonatorComponent
    ],
    declarations: [
        TopDonatorComponent
    ],
    providers: [
        InAppBrowser
    ]
})
export class TopDonatorRouteModule {
}
