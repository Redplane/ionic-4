import {Component, Inject, OnDestroy} from '@angular/core';
import {InAppBrowser, InAppBrowserEvent, InAppBrowserObject} from '@ionic-native/in-app-browser/ngx';
import {IPaymentService} from '../../services/interfaces/payment-service.interface';
import {ActionSheetController, LoadingController} from '@ionic/angular';
import {from, Subscription} from 'rxjs';
import {finalize, flatMap, tap} from 'rxjs/operators';
import {ActionSheetButton} from '@ionic/core';
import {TranslateService} from '@ngx-translate/core';
import {PAYMENT_SERVICE_INJECTION_TOKEN} from '../../constants/injection-token.constant';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'top-donator',
    templateUrl: './top-donator.component.html',
})
export class TopDonatorComponent implements OnDestroy {

    //#region Properties

    /*
    * Subscription of browser successful load event.
    * */
    private _hookBrowserCompleteLoadSubscription: Subscription;

    /*
    * Subscription of browser close event.
    * */
    private _hookBrowserCloseSubscription: Subscription;

    /*
    * In-app browser instance.
    * */
    private _browser: InAppBrowserObject;

    //#endregion

    //#region Constructor

    public constructor(protected browser: InAppBrowser,
                       @Inject(PAYMENT_SERVICE_INJECTION_TOKEN) protected paymentService: IPaymentService,
                       protected loadingController: LoadingController,
                       protected actionSheetController: ActionSheetController,
                       protected translateService: TranslateService) {
    }

    //#endregion

    //#region Methods

    /*
    * Called when component is destroyed.
    * */
    public ngOnDestroy(): void {

        if (this._hookBrowserCompleteLoadSubscription && !this._hookBrowserCompleteLoadSubscription.closed) {
            this._hookBrowserCompleteLoadSubscription.unsubscribe();
        }

        if (this._hookBrowserCloseSubscription && !this._hookBrowserCloseSubscription.closed) {
            this._hookBrowserCloseSubscription.unsubscribe();
        }

        if (this._browser) {
            this._browser.close();
        }
    }

    /*
    * Called when floating action button is clicked.
    * */
    public clickFloatingActionButton(): void {

        const donateActionSheetButton: ActionSheetButton = {
            text: this.translateService.instant('TITLE_DONATE'),
            icon: 'cash',
            handler: () => this.clickDonate()
        };

        from(this.actionSheetController
            .create({
                animated: true,
                buttons: [
                    donateActionSheetButton
                ]
            }))
            .subscribe(actionSheetContextMenu => {
                actionSheetContextMenu.present();
            });
    }

    /*
    * Called when donate button is clicked.
    * */
    public clickDonate(): void {

        // Save the instance of loading message.
        let displayingLoadingMessage: any;

        // Display loading message
        from(this.loadingController.create({}))
            .pipe(
                flatMap(loadingMessage => {

                    displayingLoadingMessage = loadingMessage;

                    // Display the loading message.
                    loadingMessage.present();

                    // Get url that payment gateway generates.
                    return this.paymentService
                        .buildPaymentRequestAsync();
                }),

                tap((paymentUrl: string) => {
                    const browserRef = this.browser.create(paymentUrl, '_self', 'location=no');

                    // Catch the event which is raised when browser completely loaded a page.
                    this._hookBrowserCompleteLoadSubscription = browserRef.on('loadstop')
                        .subscribe((event: InAppBrowserEvent) => this.handleSuccessfulPaymentRedirection(event, browserRef));

                    this._hookBrowserCloseSubscription = browserRef.on('exit')
                        .subscribe((event: InAppBrowserEvent) => {
                            console.log(event);
                        });
                }),

                finalize(() => {
                    displayingLoadingMessage.dismiss();
                })
            )
            .subscribe();
    }

    /*
    * Handle successful payment redirection.
    * */
    protected handleSuccessfulPaymentRedirection = (event: InAppBrowserEvent, browserRef: InAppBrowserObject): void => {

        // Get the redirected url.
        const redirectedUrl = event.url;

        if (this.paymentService.hasPaymentRequestSucceeded(redirectedUrl)) {
            console.log('Successful');
            // Close the browser.
            browserRef.close();
        }
    };

    //#endregion
}
