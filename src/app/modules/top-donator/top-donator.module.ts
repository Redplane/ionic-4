import {NgModule} from '@angular/core';
import {TopDonatorComponent} from './top-donator.component';
import {RouterModule, Routes} from '@angular/router';
import {TopDonatorRouteModule} from './top-donator.route';

const routes: Routes = [
    {
        path: '',
        component: TopDonatorComponent
    }
];

@NgModule({
    imports: [
        TopDonatorRouteModule,
        RouterModule.forChild(routes)
    ]
})

export class TopDonatorModule {
}
