import {RouterModule, Routes} from '@angular/router';
import {ProfileComponent} from './profile.component';
import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {IonicModule} from '@ionic/angular';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx';
import {Camera} from '@ionic-native/camera/ngx';
import {PipeModule} from '../../pipes/pipe.module';
import {Crop} from '@ionic-native/crop/ngx';

const routes: Routes = [
    {
        path: '',
        component: ProfileComponent
    }
];

@NgModule({
    imports: [
        TranslateModule,
        IonicModule,
        FormsModule,
        CommonModule,
        RouterModule.forChild(routes),
        PipeModule
    ],
    declarations: [
        ProfileComponent
    ],
    providers: [
        BarcodeScanner,
        Camera,
        Crop
    ]
})
export class ProfileRouteModule {
}
