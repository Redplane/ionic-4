import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {ProfileRouteModule} from './profile.route';


@NgModule({
    imports: [
        TranslateModule.forChild(),
        ProfileRouteModule
    ]
})
export class ProfileModule {
}
