import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx';
import {IQrLoginService} from '../../services/interfaces/qr-login-service.interface';
import {from, Observable, of, Subscription, throwError} from 'rxjs';
import {catchError, finalize, flatMap, map, tap} from 'rxjs/operators';
import {ActionSheetController, LoadingController, ModalController, Platform, ToastController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {ActionSheetButton} from '@ionic/core';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {Router} from '@angular/router';
import {Crop} from '@ionic-native/crop/ngx';
import {PlatformNameConstant} from '../../constants/platform-name.constant';
import {IUserService} from '../../services/interfaces/user-service.interface';
import {HttpClient} from '@angular/common/http';
import {QR_LOGIN_INJECTION_TOKEN, USER_SERVICE_INJECTION_TOKEN} from '../../constants/injection-token.constant';

declare var window;

/**
 * Generated class for the LoginComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'qr-login',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {

    //#region Properties

    // Subscription in the system.
    private readonly _subscriptions: Subscription;

    // Subscription which is for doing login.
    private _doQrLoginSubscription: Subscription;

    public profilePhoto = 'http://i63.tinypic.com/1y7mtd.png';

    //#endregion

    //#region Constructor

    public constructor(@Inject(QR_LOGIN_INJECTION_TOKEN) protected qrLoginService: IQrLoginService,
                       protected platform: Platform,
                       protected toastController: ToastController,
                       protected modalController: ModalController,
                       protected barcodeScanner: BarcodeScanner,
                       protected actionSheetController: ActionSheetController,
                       protected translateService: TranslateService,
                       protected cameraService: Camera,
                       protected loadingController: LoadingController,
                       protected platformService: Platform,
                       protected crop: Crop,
                       protected router: Router,
                       @Inject(USER_SERVICE_INJECTION_TOKEN) protected userService: IUserService,
                       protected httpClient: HttpClient) {
        this._subscriptions = new Subscription();
    }

    //#endregion

    //#region Methods

    // Raised when component is initialized.
    public ngOnInit(): void {
    }

    // Raised when component is destroyed.
    public ngOnDestroy(): void {
        if (this._subscriptions != null && this._subscriptions.closed) {
            this._subscriptions.unsubscribe();
        }
    }

    // Use device camera and scan for qr code asynchronously.
    protected loadQrFromCameraAsync(): Observable<string> {

        const platforms = this.platform.platforms();
        const matchedPlatforms = platforms.filter(value => ['desktop'].includes(value));
        if (matchedPlatforms && matchedPlatforms.length) {
            return of('123445');
        }

        return from(this.barcodeScanner.scan())
            .pipe(
                map((barcodeScanResult) => barcodeScanResult.text)
            );

    }

    // Called when qr scanner is started.
    public clickLoginQrCode(): void {

        let message = '';

        this._doQrLoginSubscription = this.loadQrFromCameraAsync()
            .pipe(
                flatMap((code) => {
                    message = 'Code has been scanned successfully';
                    return this.qrLoginService
                        .resolveQrLoginSessionAsync(code);
                }),
                catchError((error) => {
                    message = 'Cannot access to API server';
                    return throwError(error);
                }),
                finalize(() => {
                    this.toastController
                        .create({
                            duration: 2000,
                            position: 'top',
                            message
                        })
                        .then((addedToastMessage) => addedToastMessage.present());
                })
            )
            .subscribe();

        // Add the login subscription to watch list.
        this._subscriptions.add(this._doQrLoginSubscription);
    }

    /*
    * Raised when settings button is clicked
    * */
    public clickSettings(): void {

        // Qr login button.
        const qrLoginButton: ActionSheetButton = {
            text: this.translateService.instant('TITLE_QR_LOGIN'),
            icon: 'qr-scanner',
            handler: () => this.clickLoginQrCode()
        };

        // Upload photo button.
        const uploadPhotoButton: ActionSheetButton = {
            text: this.translateService.instant('TITLE_UPLOAD_PHOTO'),
            icon: 'cloud-upload'
        };

        // Take picture
        const takeProfilePhotoPictureButton: ActionSheetButton = {
            text: this.translateService.instant('TITLE_TAKE_PROFILE_PHOTO'),
            icon: 'camera',
            handler: () => this.captureCameraPhotoHandler()
        };


        // Display action sheet.
        this.actionSheetController
            .create({
                header: this.translateService.instant('TITLE_SETTINGS'),
                buttons: [qrLoginButton, uploadPhotoButton, takeProfilePhotoPictureButton]
            })
            .then(actionSheet => actionSheet.present());
    }

    /*
    * Handler for capture camera action sheet item.
    * */
    protected captureCameraPhotoHandler(): void {

        // Initialize camera options.
        const options: CameraOptions = {
            destinationType: this.cameraService.DestinationType.FILE_URI,
            encodingType: this.cameraService.EncodingType.JPEG,
            mediaType: this.cameraService.MediaType.PICTURE,
            targetWidth: this.platform.width(),
            targetHeight: this.platform.height(),
            correctOrientation: true
        };

        // Display loading message
        this.loadingController
            .create({
                backdropDismiss: false
            })
            .then(loadingMessage => {
                loadingMessage.present();
                return this.cameraService
                    .getPicture(options);
            })
            // .then((encodedImage: string) => {
            //     // imageData is either a base64 encoded string or a file URI
            //     // If it's base64 (DATA_URL):
            //     encodedImage = 'data:image/jpeg;base64,' + encodedImage;
            //     this.profilePhoto = encodedImage;
            //
            //     this.router
            //         .navigate(['profile', 'edit-photo'], {
            //             state: {
            //                 sourceImage: encodedImage
            //             }
            //         });
            // })
            .then((fileUri) => {
                // Crop Image, on android this returns something like, '/storage/emulated/0/Android/...'
                // Only giving an android example as ionic-native camera has built in cropping ability
                if (this.platform.is(PlatformNameConstant.ios)) {
                    return fileUri;
                } else if (this.platform.is(PlatformNameConstant.android)) {
                    // Modify fileUri format, may not always be necessary
                    fileUri = `file://${fileUri}`;

                    /* Using cordova-plugin-crop starts here */
                    return this.crop.crop(fileUri, {
                        quality: 100,
                        targetWidth: this.platform.width(),
                        targetHeight: this.platform.width()
                    });
                }
            })
            .then(fileUrl => this.uriToBlob(fileUrl))
            .then((blob) => {
                return this.userService.uploadUserPhotoAsync(blob)
                    .toPromise();
            })
            .finally(() => {
                this.loadingController.dismiss();
            });
    }

    /*
    * Convert uri to blob.
    * */
    public uriToBlob(_imagePath): Promise<Blob> {
        return new Promise<Blob>((resolve, reject) => {
            window.resolveLocalFileSystemURL(_imagePath, (fileEntry) => {

                fileEntry.file((resFile) => {

                    const reader = new FileReader();
                    reader.onloadend = (evt: any) => {
                        const imgBlob: any = new Blob([evt.target.result], {type: 'image/png'});
                        resolve(imgBlob);
                    };

                    reader.onerror = (e) => {
                        reject(e);
                    };

                    reader.readAsArrayBuffer(resFile);
                });
            });
        });

        //#endregion

    }
}
