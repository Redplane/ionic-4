export class PaymentModel {

    //#region Properties

    public transactionType: string;

    public paymentMethod: string;

    public serviceId: string;

    public paymentId: string;

    public orderNumber: string;

    public paymentDescription: string;

    public merchantReturnUrl: string;

    public amount: number;

    public currencyCode: string;

    public customerName: string;

    public customerEmail: string;

    public customerPhone: string;

    //#endregion

}
