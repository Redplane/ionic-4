export interface IHostNameChangerModalResult {
    hostname: string;
}
