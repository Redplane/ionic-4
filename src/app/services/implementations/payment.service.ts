import {IPaymentService} from '../interfaces/payment-service.interface';
import {Injectable} from '@angular/core';
import {from, Observable} from 'rxjs';
import {AppConfigService} from './app-config.service';
import {flatMap, map} from 'rxjs/operators';
import {AppConfigModel} from '../../models/app-config.model';
import {HttpClient} from '@angular/common/http';
import {PaymentModel} from '../../models/payment.model';

@Injectable()
export class PaymentService implements IPaymentService {

    //#region Properties

    private readonly _successfulPaymentResponseUrl = 'http://localhost:4300';

    //#endregion

    //#region Constructor

    public constructor(protected appConfigService: AppConfigService,
                       protected httpClient: HttpClient) {

    }

    //#endregion

    //#region Methods

    /*
    * Build payment request asynchronously.
    * */
    public buildPaymentRequestAsync(): Observable<string> {

        // Load application configuration.
        return from(this.appConfigService.loadAppSettingsAsync())
            .pipe(
                flatMap((appSettings: AppConfigModel) => {
                    const fullUrl = `${appSettings.baseUrl}/api/order/build-request`;

                    // Build multipart/form-data body.
                    const body = {
                        merchantReturnUrl: 'http://localhost:4200',
                        merchantApprovalUrl: 'http://localhost:4300',
                        merchantUnApprovalUrl: 'http://localhost:4400'
                    };

                    return this.httpClient
                        .post(fullUrl, body);
                }),
                map((model: PaymentModel) => {

                    // Get model keys.
                    const keys = Object.keys(model);
                    const parameters = keys.map(key => `${key}=${model[key]}`)
                        .join('&');

                    return `https://test2pay.ghl.com/IPGSG/Payment.aspx?${parameters}`;
                })
            );
    }

    /*
    * Whether url that user is redirected to belongs to merchantReturnUrl.
    * */
    public hasPaymentRequestSucceeded(url: string): boolean {
        if (!url) {
            return false;
        }

        if (url.length < this._successfulPaymentResponseUrl.length) {
            return this._successfulPaymentResponseUrl.indexOf(url) === 0;
        }

        return url.indexOf(this._successfulPaymentResponseUrl) === 0;
    }

    //#endregion
}
