import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppConfigModel} from '../../models/app-config.model';
import {catchError, map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {forkJoin, of} from 'rxjs';
import set = Reflect.set;

@Injectable()
export class AppConfigService {

    //#region Properties

    private _appConfiguration: AppConfigModel;

    //#endregion

    //#region Accessors

    public set setting(value: AppConfigModel) {
        if (!value) {
            this._appConfiguration = new AppConfigModel();
            return;
        }

        this._appConfiguration = value;
    }

    //#endregion

    //#region Constructors

    constructor(protected httpClient: HttpClient) {

    }

    //#endregion

    //#region Application configuration

    /*
    * Load app configuration from json file.
    * */
    public loadAppSettingsAsync(reload?: boolean): Promise<AppConfigModel> {

        if (this._appConfiguration && !reload) {
            return new Promise(resolve => resolve(this._appConfiguration));
        }

        // List of observables to be processed.
        const processingObservables = [];

        // Get appsettings file.
        processingObservables.push(this.httpClient
            .get<AppConfigModel>('/assets/appsettings.json'));

        // Get environmental appsettings file.
        if (environment.name && environment.name.length) {

            const loadEnvironmentalAppSettingsObservable = this.httpClient
                .get<AppConfigModel>(`/assets/appsettings.${environment.name}.json`)
                .pipe(
                    catchError(_ => of({}))
                );

            processingObservables.push(loadEnvironmentalAppSettingsObservable);
        }

        return forkJoin(processingObservables)
            .pipe(
                map((items: AppConfigModel[]) => {
                    let output = {};
                    for (const item of items) {
                        output = {
                            ...output,
                            ...item
                        };
                    }

                    const settings = output as AppConfigModel;
                    this._appConfiguration = settings;
                    return settings;
                })
            )
            .toPromise();
    }

    /*
    * Load configuration from cache.
    * */
    public loadConfigurationFromCache(): AppConfigModel {
        return this._appConfiguration;
    }

    //#endregion
}
