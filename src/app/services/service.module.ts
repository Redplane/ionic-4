import {NgModule, ModuleWithProviders} from '@angular/core';
import {MessageBusService} from './implementations/message-bus.service';
import {QrLoginService} from './implementations/qr-login.service';
import {UserService} from './implementations/user.service';
import {PaymentService} from './implementations/payment.service';
import {
    PAYMENT_SERVICE_INJECTION_TOKEN,
    QR_LOGIN_INJECTION_TOKEN,
    USER_SERVICE_INJECTION_TOKEN
} from '../constants/injection-token.constant';

@NgModule()
export class ServiceModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ServiceModule,
            providers: [
                {provide: 'IMessageBusService', useClass: MessageBusService},
                {provide: QR_LOGIN_INJECTION_TOKEN, useClass: QrLoginService},
                {provide: USER_SERVICE_INJECTION_TOKEN, useClass: UserService},
                {provide: PAYMENT_SERVICE_INJECTION_TOKEN, useClass: PaymentService}
            ]
        };
    }
}
