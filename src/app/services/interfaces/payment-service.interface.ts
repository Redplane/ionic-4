import {Observable} from 'rxjs';

export interface IPaymentService {

    //#region Methods

    /*
    * Build payment request url asynchronously.
    * */
    buildPaymentRequestAsync(): Observable<string>;

    /*
    * Whether url that user is redirected to belongs to merchantReturnUrl.
    * */
    hasPaymentRequestSucceeded(url: string): boolean;

    //#endregion
}
