import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {SplashScreen} from "@ionic-native/splash-screen/ngx";
import {AppComponent} from './app.component';
import {IonicModule, IonicRouteStrategy} from "@ionic/angular";
import {RouteReuseStrategy} from "@angular/router";
import {AppConfigService} from "./services/implementations/app-config.service";
import {appConfigServiceFactory} from "./factories/app-config.factory";
import {AppRouteModule} from "./app.route";
import {SharedModule} from "./modules/shared.module";

@NgModule({
    entryComponents: [],
    declarations: [
        AppComponent
    ],
    imports: [
        IonicModule.forRoot(),
        BrowserModule,
        AppRouteModule,
        SharedModule,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        AppConfigService,
        {
            provide: APP_INITIALIZER,
            useFactory: appConfigServiceFactory,
            multi: true,
            deps: [AppConfigService]
        },
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    bootstrap: [AppComponent]
})

//#endregion

export class AppModule {
}
