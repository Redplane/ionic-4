export class DialogButtonRoleConstant {

    //#region Properties

    public static ok = 'ok';

    public static cancel = 'cancel';

    //#endregion

}
