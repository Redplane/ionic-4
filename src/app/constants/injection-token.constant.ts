import {InjectionToken} from '@angular/core';
import {IUserService} from '../services/interfaces/user-service.interface';
import {IPaymentService} from '../services/interfaces/payment-service.interface';
import {IQrLoginService} from '../services/interfaces/qr-login-service.interface';

// Injection token of user service.
export const USER_SERVICE_INJECTION_TOKEN = new InjectionToken<IUserService>('Injection token of user service');

// Injection token of payment service.
export const PAYMENT_SERVICE_INJECTION_TOKEN = new InjectionToken<IPaymentService>('Injection token of payment service');

// Injection token of qr login service.
export const QR_LOGIN_INJECTION_TOKEN = new InjectionToken<IQrLoginService>('Injection token of qr login service.');
