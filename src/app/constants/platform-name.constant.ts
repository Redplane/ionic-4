export class PlatformNameConstant {

    //#region Properties

    public static readonly android = 'android';

    public static readonly ios = 'ios';

    //#endregion

}
