export class StorageKeyConstant {

    //#region Properties

    /*
    * Key to store access token.
    * */
    public static accessToken = 'access-token';

    /*
    * Username that is used for sign in purpose.
    * */
    public static signedInUsername = 'signed-in-username';

    //#endregion

}
