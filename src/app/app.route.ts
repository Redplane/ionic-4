import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginModule} from './modules/login/login.module';
import {AppStartModule} from './modules/app-start/app-start.module';
import {ProfileResolve} from './resolves/profile.resolve';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpRequestInterceptor} from './interceptors/http-request.interceptor';
import {LoginGuard} from './guards/login.guard';
import {HttpResponseInterceptor} from './interceptors/http-response.interceptor';
import {IonicModule} from '@ionic/angular';
import {SharedModule} from './modules/shared.module';

const routes: Routes = [
    // {
    //   path: 'about-us',
    //   loadChildren: './modules/about-us/about-us.module#AboutUsModule'
    // },
    {
        path: 'app-start',
        loadChildren: () => import('./modules/app-start/app-start.module').then(m => m.AppStartModule)
    },
    // {
    //   path: 'child-profile',
    //   loadChildren: './modules/child-profile/child-profile.module#ChildProfileModule'
    // },
    // {
    //   path: 'guardian-profile',
    //   loadChildren: './modules/guardian-profile/guardian-profile.module#GuardianProfileModule'
    // },
    {
        path: 'login',
        canActivate: [LoginGuard],
        loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule)
    },
    {
        path: '',
        resolve: {
            profile: ProfileResolve
        },
        children: [
            {
                path: 'main',
                loadChildren: () => import('./modules/main/main.module').then(m => m.MainModule)
            },
            {
                path: 'profile',
                loadChildren: () => import('./modules/profile/profile.module').then(m => m.ProfileModule)
            }
        ]
    },
    // {
    //   path: 'my-favorites',
    //   loadChildren: './modules/my-favorite/my-favorite.module#MyFavoriteModule'
    // },
    // {
    //   path: 'my-track-records',
    //   loadChildren: './modules/my-track-records/my-track-records.module#MyTrackRecordsModule'
    // },
    {
        path: 'top-donator',
        loadChildren: () => import('./modules/top-donator/top-donator.module').then(m => m.TopDonatorModule)
    },
    // {
    //   path: 'volunteer',
    //   loadChildren: './modules/volunteer/volunteer.module#VolunteerModule'
    // }
    {
        path: '**',
        redirectTo: 'app-start'
    }
];

@NgModule({
    imports: [
        SharedModule,
        AppStartModule,
        LoginModule,
        RouterModule.forRoot(routes, {enableTracing: false})
    ],
    providers: [
        ProfileResolve,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpRequestInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpResponseInterceptor,
            multi: true
        },
        LoginGuard
    ],
    exports: [
        RouterModule
    ]
})

export class AppRouteModule {
}
