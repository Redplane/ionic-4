import {Pipe} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

// tslint:disable-next-line:use-pipe-transform-interface
@Pipe({name: 'toTrustedUrl'})
export class ToTrustedUrlPipe {

    //#region Constructor

    constructor(private sanitizer: DomSanitizer) {
    }

    //#endregion

    //#region Methods

    transform(style) {
        return this.sanitizer.bypassSecurityTrustUrl(style);
    }

    //#endregion
}
